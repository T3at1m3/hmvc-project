<?php

namespace App\Core\Interfaces;


/**
 * The singleton interface
 */
interface SingletonInterface
{
    public static function getInstance();
}
