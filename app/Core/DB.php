<?php

namespace App\Core;

use App\Core\Interfaces\SingletonInterface;

/**
 * The DB abstraction
 */
class DB implements SingletonInterface
{
    private static $driverName = "PDOMySQL";
    private static $driverNamespace = "App\Core\Drivers\\";
    private static $instance;

    public static function getInstance()
    {
        if(self::$instance !== null) {
            return self::$instance;
        }

        $class = self::$driverNamespace . self::$driverName;

        if(!class_exists($class)) {
            throw new \Exception("Cannot instantiate DB Driver. Class doesn't exist: {$class}");
            return false;
        }

        $instance = $class::getInstance();

        self::$instance = $instance;

        return self::$instance;
    }

    public static function setDriver($driver)
    {
        self::$driverName = $driver;
    }
}