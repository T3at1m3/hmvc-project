<?php

namespace App\Core;

/**
 * The main Router object, that handles and dispatches all the requests
 *
 * @package Titan\App\Core\
 * @author Al.Takev <al.takev@gmail.com>
 * @version 0.0.1
 */
class Router
{
    private $module;
    private $controller;
    private $method;
    private $arg;
    private $controllerInstance = array();

    /**
     * Constructor
     *
     * Add instance from App/Core/Module here and pass the objects when needed
     */
    public function __construct($url)
    {
        $url = explode('/', $url);
        array_shift($url);
        $urlCount         = count($url);
        $controller       = !empty($url[0]) ? $url[0] . 'Controller' : 'HomeController';
        $this->controller = ucfirst($controller);

        $this->module = ucfirst(str_replace('Controller', '\\', $controller));

        $method = !empty($url[1]) ? $url[1] : 'index';

        $this->method = $method;

        $arg = !empty($url[2]) ? $url[2] : null;

        if ($urlCount > 3) {
            $arg   = array();
            $arg[] = $url[2];
            $index = $urlCount - ($urlCount - 3);
            for ($i = $index; $i < $urlCount; $i++) {
                $arg[] = $url[$i];
            }
        }
        $this->arg = $arg;

        $this->dispatch();
    }

    /**
     * Returns the instantiated controller with his method and arguments as
     * parameters
     * @return object Instance of the requested controller->method pair
     */
    private function dispatch()
    {
        $controller = "App\Modules\\" . $this->module . "Controller\\" . $this->controller;

        if (!isset($this->controllerInstance[$controller])) {
            if (!class_exists($controller)) {
                throw new \Exception("Call to invalid controller: " . $controller);
            }

            $this->controllerInstance[$controller] = new $controller;

            $method = $this->method;
            if (!is_callable(array($this->controllerInstance[$controller], $method))) {
                throw new \Exception('Cannot call method');
            }
        }
        $arg = $this->arg;

        return $this->controllerInstance[$controller]->$method($arg);
    }

    /**
     * Getter for the Controller
     * @return string controller name
     */
    public static function getController()
    {
        return $this->controller;
    }

    /**
     * The method called by the request
     * @return string method name
     */
    public static function getMethod()
    {
        return $this->method;
    }

    /**
     * The arguments passed to the controller->method() pair
     * @return array
     */
    public static function getArg()
    {
        return $this->arg;
    }
}
