<?php

namespace App\Core;

/**
 * The main Session Class
 *
 * @package Titan\App\Core\
 * @author Al.Takev <al.takev@gmail.com>
 * @version 0.0.1
 */
class Session
{
    /**
     * Sets a key -> value pair to the $_SESSION array
     * @param string $key
     * @param mixed $value
     */
    public static function set($key, $value)
    {
        $_SESSION[$key] = $value;
    }

    /**
     * Gets the value from the $_SESSION array based on the given $key
     * @param  string $key
     * @return mixed | null The value in the $key->$value pair
     */
    public static function get($key)
    {
        if(isset($_SESSION[$key])) {
            return $_SESSION[$key];
        }
        return null;
    }

    /**
     * Deletes the $_SESSION $value based on the given $key
     * @param  string $key
     */
    public static function delete($key)
    {
        if( isset($_SESSION[$key]))
        {
            unset($_SESSION[$key]);
        }
    }

    /**
     * Initializes the $_SESSION array
     */
    public static function start()
    {
        session_start();
    }

    /**
     * Destroys the current $_SESSION array
     */
    public static function destroy()
    {
        session_destroy();
    }
}