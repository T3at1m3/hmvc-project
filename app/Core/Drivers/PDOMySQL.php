<?php

namespace App\Core\Drivers;

use App\Core\Interfaces\SingletonInterface;

/**
 * The data is sent separately, clearly understood as being data and data
 * only. The db engine doesn't even try to analyze the content of the data
 * string to see if it contains instructions, and any potentially damaging
 * code snipet is not considered.
 */

class PDOMySQL implements SingletonInterface
{
    public static $connectionOptions = [
        'host'     => 'localhost',
        'port'     => 3306,
        'user'     => null,
        'pass'     => null,
        'database' => null,
        'charset'  => "utf8",
    ];

    private static $instance;
    private $pdo;
    private $stmt;
    private $sql;
    private $params;

    /**
     * Singleton
     */
    public static function getInstance()
    {
        return isset(static::$instance) ? static::$instance : static::$instance = new static();
    }

    /**
     * Set the options for the database connection
     *
     * @param array $options
     */
    public static function setOption(array $options)
    {
        if (self::$instance !== null) {
            return false;
        }

        /** Host */
        if (isset($options['host'])) {
            self::$connectionOptions['host'] = $options['host'];
        }

        /** User */
        if (isset($options['user'])) {
            self::$connectionOptions['user'] = $options['user'];
        }

        /** Password */
        if (isset($options['pass'])) {
            self::$connectionOptions['pass'] = $options['pass'];
        }
        /** Database */
        if (isset($options['database'])) {
            self::$connectionOptions['database'] = $options['database'];
        }
        /** port */
        if (isset($options['port'])) {
            self::$connectionOptions['port'] = $options['port'];
        }
        /** Charset */
        if (isset($options['charset'])) {
            self::$connectionOptions['charset'] = $options['charset'];
        }

        return true;
    }

    private function __construct(array $connectionOptions = array())
    {
        $this->prepareConnection($connectionOptions);
        $this->dbConnect();
    }

    /**
     * Prepare the connection or throw error
     * @param  array $connectionOptions DB config
     */
    private function prepareConnection($connectionOptions)
    {
        if ($connectionOptions) {
            //host
            if (!isset($connectionOptions['host'])) {
                throw new \Exception("Host not set");
            } else {
                self::$connectionOptions['host'] = $connectionOptions['host'];
            }
            //user
            if (!isset($connectionOptions['user'])) {
                throw new \Exception("User not set");
            } else {
                self::$connectionOptions['user'] = $connectionOptions['user'];
            }
            //pass
            if (!isset($connectionOptions['pass'])) {
                throw new \Exception("Password not set");
            } else {
                self::$connectionOptions['pass'] = $connectionOptions['pass'];
            }
            //database
            if (!isset($connectionOptions['database'])) {
                throw new \Exception("Database not set");
            } else {
                self::$connectionOptions['database'] = $connectionOptions['database'];
            }
            //charset
            if (!isset($connectionOptions['port'])) {
                self::$connectionOptions['port'] = '3306';
            } else {
                self::$connectionOptions['port'] = $connectionOptions['port'];
            }
            //charset
            if (!isset($connectionOptions['charset'])) {
                self::$connectionOptions['charset'] = 'utf8';
            } else {
                self::$connectionOptions['charset'] = $connectionOptions['charset'];
            }
        }
    }

    /**
     * Creates the connection
     */
    private function dbConnect()
    {
        /** DNS */
        $dsn = 'mysql:host=' . self::$connectionOptions['host'] . ';port=' . self::$connectionOptions['port'] . ';dbname=' . self::$connectionOptions['database'];

        /** Options */
        $options = array(
            \PDO::ATTR_PERSISTENT         => true,
            \PDO::MYSQL_ATTR_INIT_COMMAND => "set names " . self::$connectionOptions['charset'],
            \PDO::ATTR_ERRMODE            => \PDO::ERRMODE_EXCEPTION,
            \PDO::ATTR_EMULATE_PREPARES   => false,
        );
        try {
            /** Connect */
            $this->pdo = new \PDO($dsn, self::$connectionOptions['user'], self::$connectionOptions['pass'], $options);
        } catch (\PDOException $e) {
            /** Can't Connect */
            echo "Can't connect to PDOMySQL : {" . self::$connectionOptions['host'] . "}<br />\n" . $e->getMessage();
        }
    }

    /**
     * The prepared statement to be executed by the PDO object
     * @param  string $sql    SQL statement
     * @param  array  $params Prepared parameters needed for the prepared
     *                        statement
     * @return object         The PDO object
     */
    public function prepare($sql, $params = array())
    {
        $this->stmt   = $this->pdo->prepare($sql);
        $this->params = $params;
        $this->sql    = $sql;
        return $this;
    }

    /**
     * The execute method, executes the prepared statement
     * @return object The PDO object
     */
    public function execute()
    {
        $this->stmt->execute($this->params);
        return $this;
    }

    /**
     * Returns the results of the executed query as an associative array
     * @param  boolean $assoc If the param is false, returns the array with
     *                        numerical values as keys
     * @return array
     */
    public function fetchAllAssoc($assoc = true)
    {
        if ($assoc) {
            return $this->stmt->fetchAll(\PDO::FETCH_ASSOC);
        } else {
            return $this->stmt->fetchAll(\PDO::FETCH_NUM);
        }
    }

    /**
     * Returns the result of the single executed query as an associative array
     * @return array
     */
    public function fetchRowAssoc($assoc = true)
    {
        if ($assoc) {
            return $this->stmt->fetch(\PDO::FETCH_ASSOC);
        } else {
            return $this->stmt->fetch(\PDO::FETCH_NUM);
        }

    }

    /**
     * Fetches the result based on the $column parameter
     * @param  string $column The desired column from a table in SQL
     * @return array         This array holds all the column data
     */
    public function fetchAllColumn($column)
    {
        return $this->stmt->fetchAll(\PDO::FETCH_COLUMN, $column);
    }

    /**
     * Fetches a single row of the selected column
     * @param  string $column The desired column from a table in SQL
     * @return array         The requested row
     */
    public function fetchRowColumn($column)
    {
        return $this->stmt->fetch(\PDO::FETCH_COLUMN, $column);
    }

    /**
     * The last inserted ID of an INSERT SQL query
     * @return string
     */
    public function lastInsertID()
    {
        return $this->pdo->lastInsertId();
    }

    /**
     * Counts the affected rows from the last query
     * @return integer The row count
     */
    public function rowCount()
    {
        return $this->stmt->rowCount();
    }
}
