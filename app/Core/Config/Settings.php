<?php

namespace App\Core\Config;

use App\Core\DB;
use App\Core\Drivers\PDOMySQL;
use App\Core\Module;
use Faker\Factory;

/**
 * General Settings object
 *
 * Creates the database
 */

class Settings
{
    private $defaultUser = null;
    private $databaseName;
    private $db;
    private $tables = [
        'user',
        'article',
    ];

    public function __construct($driver, $connectionOptions)
    {
        $this->setDBConnection($driver, $connectionOptions);
    }

    private function setDBConnection($driver, $connectionOptions)
    {
        DB::setDriver($driver);
        PDOMySQL::setOption($connectionOptions);
        $this->db = DB::getInstance();
    }

    /**
     * Creates the users from the config.php file
     * Than activates them.
     *
     * @param array   $userData The user data, to be created
     * @param boolean $status
     */
    public function setDefaultUser($userData, $status)
    {
        if ($status === true) {
            $module     = new Module();
            $usersModel = $module->getModelInstance('User');
            $status     = [];
            $i          = 0;
            foreach ($userData as $user) {
                $status[$i] = $usersModel->add($user);
                $i++;
            }

            $sql = "SELECT * FROM user";
            $this->db->prepare($sql)->execute();

            $users = $this->db->fetchAllAssoc();

            foreach ($users as $user) {
                $sql = "UPDATE user
                            SET active = 1,
                                code   = ''
                        WHERE
                            id = :id";
                $params = [
                    'id' => $user['id'],
                ];

                $this->db->prepare($sql, $params)->execute();
            }
        }

    }

    private function createUserTable()
    {
        $sql = "CREATE TABLE IF NOT EXISTS `user` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `username` varchar(80) NOT NULL,
            `email` varchar(80) NOT NULL,
            `password` char(80) NOT NULL,
            `active` tinyint(1) NOT NULL DEFAULT 0,
            `code` varchar(80) NOT NULL,
            `role` enum('admin','editor') NOT NULL DEFAULT 'editor',
            `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
            `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            PRIMARY KEY (`id`),
            UNIQUE KEY `email` (`email`)
        ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;";

        $this->db->prepare($sql)->execute();
    }

    private function createArticleTable()
    {
        $sql = "CREATE TABLE IF NOT EXISTS `article` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `title` varchar(255) NOT NULL,
            `text` text NOT NULL,
            `user_id` int(11) NOT NULL COMMENT 'author',
            `image` varchar(80) NOT NULL,
            `active` tinyint(1) NOT NULL,
            `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
            `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            PRIMARY KEY (`id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;";

        $this->db->prepare($sql)->execute();
    }

    public function setDBTabels($status = false)
    {
        $this->databaseName = PDOMySQL::$connectionOptions['database'];

        if ($status == true) {
            $databaseName = $this->databaseName;
            foreach ($this->tables as $table) {
                $sql = "SELECT COUNT(*)
                        FROM information_schema.tables
                        WHERE table_schema = '{$databaseName}'
                        AND table_name = '{$table}'";
                $this->db->prepare($sql)->execute();
                $result = $this->db->fetchRowAssoc(false);
                if ($result[0] !== '1') {
                    $method = 'create' . ucfirst($table) . 'Table';
                    $this->$method();
                }
            }
        } else {
            return false;
        }
    }

    public function populatearticlesTable($status = false)
    {
        if ($status == false) {
            return;
        }
        $module = new Module();
        $faker  = Factory::create();

        $fakeArticles = [];

        for ($i = 0; $i < 20; $i++) {
            $userID           = ($i <= 9) ? 1 : 2;
            $fakeArticles[$i] = [
                'title'         => $faker->sentence,
                'article_body'  => $faker->paragraphs(5, true),
                'user_id'       => $userID,
                'article_image' => 'placeholder.jpg',
            ];
        }

        $articleModel = $module->getModelInstance('Article');

        foreach ($fakeArticles as $article) {
            $articleModel->add($article);
        }
    }
}
