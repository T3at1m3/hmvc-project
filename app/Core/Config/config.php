<?php

use App\Core\Config\Settings;

date_default_timezone_set('Europe/Sofia');
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$project = 'titan';

define('DS', DIRECTORY_SEPARATOR);
define('PATH', realpath(dirname(__FILE__)) . DS . '..' . DS . '..' . DS . '..');
define('PROJECT', 'http://' . $_SERVER['SERVER_NAME'] . DS . $project . DS);
define('PROJECT_DIR', $project . DS);
define('PUBLIC_ROOT', 'http://' . $_SERVER['SERVER_NAME'] . DS . $project . DS . 'public' . DS);
define('MEDIA', PUBLIC_ROOT . 'uploads/');
define('IMG', PUBLIC_ROOT . 'images/');

/**
 * Set the driver, and the connection options to the settings class in order
 * to jumpstart the application
 */

$driver = 'PDOMySQL';

$connectionOptions = [
    "host"     => 'localhost',
    "user"     => 'titan',
    "pass"     => 'titanpass1234',
    "database" => 'titan',
    "charset"  => "utf8",
];

$settings = new Settings($driver, $connectionOptions);

/**
 * Set DB tables creates the tables needed for the application to run. When the
 * app is already running, set the function param to false
 */
$settings->setDBTabels(false);

/**
 * SET the default User data for logging and posting in the application
 *
 * Your password must have:
 *                       1. At least one Upper Letter;
 *                       2. At least one Lower Letter;
 *                       3. At least one numeric symbol;
 *                       4. At least one special symbol.
 *
 * Email must be your real email in order to recieve a validation message
 * Else you need to manually change the users table:
 *     set active to 1
 *     set code to '' (empty string)
 */
$userData = [
    [
        'username'              => 'admin',
        'email'                 => 'al.takev@gmail.com',
        'password'              => 'Pass1234@@',
        'password-confirmation' => 'Pass1234@@',
    ],
    [
        'username'              => 'editor',
        'email'                 => 'example@example.com',
        'password'              => 'Pass1234@@',
        'password-confirmation' => 'Pass1234@@',
    ]
];

/**
 * If the second parameter is true, it will populate the default user field in
 * the users table
 * After the first run, set it to false or "Integrity constraint violation"
 * will appear
 */
$settings->setDefaultUser($userData, false);

/**
 * Populate the article table with dummy data
 */
$settings->populatearticlesTable(false);
