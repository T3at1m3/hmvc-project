<?php

namespace App\Core\Config;

/**
 * A static class to get the validator options needed for the
 * \Lib\TexValidator to compare against
 */

class ValidatorRules
{
    private static $rules = array(
        'registration' => array(
            'username'              => 'required',
            'email'                 => 'required|email',
            'password'              => 'required|password',
            'password-confirmation' => 'same:password|required',
        ),
        'login'        => [
            'user'     => 'required',
            'password' => 'required',
        ],
        'article'      => [
            'title'        => 'required',
            'article_body' => 'required',
        ],
    );

    public static function getRules($ruleName)
    {
        if (isset(self::$rules[$ruleName])) {
            return self::$rules[$ruleName];
        } else {
            return false;
        }
    }
}
