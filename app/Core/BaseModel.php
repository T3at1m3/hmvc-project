<?php

namespace App\Core;

/**
 * The base model
 *
 * Used to pass the database dependency (Dependency Injection)
 */
class BaseModel
{
    public $db;

    public function __construct()
    {
        $this->db = DB::getInstance();
    }

}