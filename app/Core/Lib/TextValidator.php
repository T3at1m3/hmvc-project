<?php

namespace App\Core\Lib;

/**
 * TextValidator class is a class that validates all the added text fields from
 * a form, that needs to be validated. The fields array is compared against an
 * array, which holds the rules and the data is validated against them.
 *
 * @uses accepted       Validating everything which can hold values like true,
 *                       false, yes, no, 1 or 0.
 * @uses minLength      Sets field minimum length.
 * @uses maxLength      Sets field maximum length.
 * @uses numeric        Checks if the field has only numeric chars.
 * @uses alpha          Checks if the field has only alphabetical chars.
 * @uses alphaNumeric   Checks if the field has only numeric and alphabetical
 *                       characters.
 * @uses firstUpper     Checks if the beginning of the fields starts with
 *                       first upper char.
 * @uses email          Checks if the field value meets the email requirements.
 * @uses password       A preg_match rule for validating a password:
 *                       1. At least one Upper Letter;
 *                       2. At least one Lower Letter;
 *                       3. At least one numeric symbol;
 *                       4. At least one special symbol.
 * @uses same           Define that the value of this field is the same as the
 *                       other. Used for password checks.
 * @uses required       If the field is required.
 * @uses isIP           If the field stands for the requirements to be a valid
 *                       IP.
 * @uses isURL          If the field stands for the requirements to be a valid
 *                       URL string.
 * @uses between        Checks if the field value meets the requirement of
 *                       being between two defined values.
 * @uses isBool         Checks the field value for boolean.
 * @uses noWhiteSpace   Checks if the string has a white space. Use only when
 *                       you don't need white spaces into the field value.
 * @uses isDate         Checks if the string is in the Date format: YYYY-MM-DD.
 * @uses isJSON         Checks if the string is validated as JSON String.
 * @uses checkEncoding  Checks if the string meets the defined encoding.
 * @uses regEx          Validates the string against the given regular
 *                       expression.
 *
 *
 * @package     Core/Lib/
 * @author      Aleksandar Takev <al.takev@gmail.com>
 * @version     0.0.1
 * @since       0.0.1
 */
class TextValidator
{
    private $validatorErrors = array(
        'accepted'      => 'Not accepted',
        'minLength'     => "This field must be at least %s chars long!",
        'maxLength'     => 'This field must be less than %s chars long!',
        'numeric'       => 'This field must contain only numeric symbols!',
        'alpha'         => 'This field must contain only alphabetical characters',
        'alphaNumeric'  => 'This field must contain only letters and numbers!',
        'firstUpper'    => 'This field must have: first letter capital, others to be lower case!',
        'email'         => 'This is an email field, it must contain email formatting: example@domain.com',
        'password'      => 'The %s field must contain: At least one capital letter, at least one lower-case letter, at least one number, and at least one special symbol.',
        'same'          => 'The field does not match the %s value',
        'required'      => 'The field is required!',
        'isIP'          => 'The field must be in the valid IP format!',
        'isURL'         => 'The field must be in the valid URL format!',
        'between'       => 'This field must be between %s and %s!',
        'isBool'        => 'The field must be boolean!',
        'noWhiteSpace'  => 'This field contains white spaces!',
        'isDate'        => 'The field must be in the correct date format: YYYY-MM-DD',
        'isJSON'        => 'The field must be in JSON format',
        'checkEncoding' => 'The field must be in the proper encoding format!',
        'regEx'         => 'The Reg Ex does not match!',
    );

    /**
     * The array holding the form data
     * @var array
     */
    private $currentFormData;

    /**
     * The validator rules onto which the $currentFormData will be validated
     * @var array
     */
    private $validatorRules;

    /**
     * The array holding the returned errors if any
     * @var array
     */
    private $errors = array();

    /**
     * Accepts 3 array arguments and initializes the validation process
     * @param array $formData               The data from the form
     * @param array $validatorRules         The rules
     * @param array  $customValidatorErrors If we wish to create custom
     *                                      messages
     */
    public function __construct($formData, $validatorRules, $customValidatorErrors = array())
    {
        $this->currentFormData = $this->sanitizeInput($formData);
        $this->validatorRules  = $validatorRules;
        foreach ($customValidatorErrors as $key => $value) {
            if (array_key_exists($key, $this->validatorErrors)) {
                $this->validatorErrors[$key] = $value;
            }
        }
        $this->errors = array();
    }

    /**
     * Initial check
     *
     * Gets the values of the $validatorRules, explodes them to array elements
     * and finds if there is a corresponding method initialize the check.
     */
    public function check()
    {
        $status = true;
        foreach ($this->currentFormData as $formDataKey => $formDataValue) {
            if (isset($this->validatorRules[$formDataKey])) {
                /** If the input is a multidimensional array */
                if (is_array($this->validatorRules[$formDataKey])) {
                    foreach ($this->validatorRules[$formDataKey] as $key => $value) {
                        $newRules[$key] = $value;
                        $rules          = explode('|', $newRules[$key]);
                    }
                    foreach ($formDataValue as $value) {
                        $formDataValue = $value;
                    }
                } else {
                    $rules = explode('|', $this->validatorRules[$formDataKey]);
                }
                $this->setEmptyErros($formDataKey);
                // Validation process
                foreach ($rules as $rule) {
                    $ruleElement = explode(":", $rule, 2);
                    $method      = $ruleElement[0];
                    unset($ruleElement[0]);
                    $ruleElement = array_values($ruleElement);

                    $result = $this->$method($formDataKey, $formDataValue, $ruleElement);

                    $status = $status && $result;
                }
            }
        }
        return $status;
    }

    /**
     * Clears the current error[$key] value
     * @param string $key The key
     */
    private function setEmptyErros($key)
    {
        $this->errors[$key] = array();
    }

    /**
     * Filtered input data (Sanitized)
     *
     * @param  array $formData
     * @return array
     */
    private function sanitizeInput($formData)
    {
        $sanitizedData = filter_var_array($formData, FILTER_SANITIZE_STRING);
        return $sanitizedData;
    }

    /**
     * Checks for accepted values
     *
     * @param string $key
     * @param string $value
     */
    private function accepted($key, $value)
    {
        $acceptedValues = array('1', 'yes', 'on', true, 1);
        if (in_array($value, $acceptedValues, true)) {
            return true;
        } else {
            $this->errors[$key]['accepted'] = $this->validatorErrors['accepted'];
            return false;
        }
    }

    /**
     * Checks for the minimal value
     *
     * @param  string $value
     * @param  array  $validateData
     * @return boolean
     */
    private function minLength($key, $value, array $validateData)
    {
        if (strlen($value) >= $validateData[0]) {
            return true;
        } else {
            $this->errors[$key]['minLength'] = sprintf($this->validatorErrors['minLength'], $validateData[0]);
            return false;
        }
    }

    /**
     * Checks for the maximum value
     *
     * @param  string $value
     * @param  array  $validateData
     * @return boolean
     */
    private function maxLength($key, $value, array $validateData)
    {
        if (strlen($value) <= $validateData[0]) {
            return true;
        } else {
            $this->errors[$key]['maxLength'] = sprintf($this->validatorErrors['maxLength'], $validateData[0]);
            return false;
        }
    }

    /**
     * Checks for numeric values
     *
     * @param  string $value
     * @param  array  $validateData
     * @return boolean
     */
    private function numeric($key, $value)
    {
        $status = is_numeric($value);
        if ($status == false) {
            $this->errors[$key]['numeric'] = $this->validatorErrors['numeric'];
        }
        return $status;
    }

    /**
     * Checks for alphabetical values
     *
     * @param  string $value
     * @return boolean
     */
    private function alpha($key, $value)
    {
        $result = ctype_alpha($value);
        if ($result == false) {
            $this->errors[$key]['alpha'] = $this->validatorErrors['alpha'];
        }
        return $result;
    }

    /**
     * Checks for both alphabetical and numeric values
     *
     * @param  string $value
     * @return boolean
     */
    private function alphaNumeric($key, $value)
    {
        $result = ctype_alnum($value);
        if ($result == false) {
            $this->errors[$key]['alphaNumeric'] = $this->validatorErrors['alphaNumeric'];
        }
        return $result;
    }

    /**
     * Checks for first upper letter
     *
     * @param  string $value
     * @return boolean
     */
    private function firstUpper($key, $value)
    {
        $comparison = ucwords(strtolower($value));
        if ($comparison === $value) {
            return true;
        } else {
            $this->errors[$key]['firstUpper'] = $this->validatorErrors['firstUpper'];
            return false;
        }
    }

    /**
     * Checks if the field is a valid email
     *
     * @param  string $value
     * @return boolean
     */
    private function email($key, $value)
    {
        if (filter_var($value, FILTER_VALIDATE_EMAIL)) {
            return true;
        } else {
            $this->errors[$key]['email'] = $this->validatorErrors['email'];
            return false;
        }
    }

    /**
     * Checks if the password meets the requirements
     *
     * @param  string $value
     * @return boolean
     */
    private function password($key, $value)
    {
        if (!preg_match('/^(?=.*?[A-Z])(?=(.*[a-z]){1,})(?=(.*[\d]){1,})(?=(.*[\W]){1,})(?!.*\s).{8,}$/', $value)) {
            $this->errors[$key]['password'] = sprintf($this->validatorErrors['password'], $key);
            return false;
        } else {
            return true;
        }
    }

    /**
     * Checks if the field value is the same as another field value
     *
     * @param  string $value
     * @param  string $validateData
     * @return boolean
     */
    private function same($key, $value, $validateData)
    {
        if ($value === $this->currentFormData[$validateData[0]]) {
            return true;
        } else {
            $this->errors[$key]['same'] = sprintf($this->validatorErrors['same'], $validateData[0]);
            return false;
        }
    }

    /**
     * Checks if the field is required
     *
     * @param  string $value
     * @return boolean
     */
    private function required($key, $value)
    {
        if (empty($value)) {
            $this->errors[$key]['required'] = $this->validatorErrors['required'];
            return false;
        } else {
            return true;
        }
    }

    /**
     * Checks if the field is in the valid IP format
     *
     * @param  string  $key
     * @param  string  $value
     * @return boolean
     */
    private function isIP($key, $value)
    {
        if (filter_var($value, FILTER_VALIDATE_IP) !== false) {
            return true;
        } else {
            $this->errors[$key]['isIP'] = $this->validatorErrors['isIP'];
            return false;
        }
    }

    /**
     * Checks if the field is in the valid URL format
     *
     * @param  string  $key
     * @param  string  $value
     * @return boolean
     */
    private function isURL($key, $value)
    {
        if (filter_var($value, FILTER_VALIDATE_URL) !== false) {
            return true;
        } else {
            $this->errors[$key]['isURL'] = $this->validatorErrors['isURL'];
            return false;
        }
    }

    /**
     * Checks if the field value length is between two values
     *
     * @param  string $key
     * @param  string $value
     * @param  array  $validateData
     * @return boolean
     */
    private function between($key, $value, array $validateData)
    {
        $valueToCheck = explode(',', $validateData[0]);
        $minValue     = array($valueToCheck[0]);
        $maxValue     = array($valueToCheck[1]);
        $min          = $this->minLength($key, $value, $minValue);
        $max          = $this->maxLength($key, $value, $maxValue);
        if (($min == true) && ($max == true)) {
            return true;
        } else {
            $this->errors[$key]['between'] = sprintf($this->validatorErrors['between'], $minValue[0], $maxValue[0]);
            return false;
        }
    }

    /**
     * Checks if the field is a boolean value
     *
     * @param  string  $key
     * @param  mixed  $value
     * @return boolean
     */
    private function isBool($key, $value)
    {
        if (filter_var($value, FILTER_VALIDATE_BOOLEAN)) {
            return true;
        } else {
            $this->errors[$key]['isBool'] = $this->validatorErrors['isBool'];
            return false;
        }
    }

    /**
     * Checks for white spaces
     *
     * @param  string $key
     * @param  string $value
     * @return boolean
     */
    private function noWhiteSpace($key, $value)
    {
        if (!preg_match('/\s/', $value)) {
            return true;
        } else {
            $this->errors[$key]['noWhiteSpace'] = $this->validatorErrors['noWhiteSpace'];
            return false;
        }
    }

    /**
     * Checks if the field is in YYYY-MM-DD Format
     *
     * @param  string  $key
     * @param  string  $value
     * @return boolean
     */
    private function isDate($key, $value)
    {
        $date = DateTime::createFromFormat('Y-m-d', $value);
        if ($date && ($date->format('Y-m-d') == $value)) {
            return true;
        } else {
            $this->errors[$key]['isDate'] = $this->validatorErrors['isDate'];
            return false;
        }
    }

    /**
     * Check if the field value is a valid JSON
     *
     * @param  string  $key
     * @param  string  $value
     * @return boolean
     */
    private function isJSON($key, $value)
    {
        if (is_string($value) && json_decode($value, true)) {
            return true;
        } else {
            $this->errors[$key]['isJSON']      = $this->validatorErrors['isJSON'];
            $this->errors[$key]['JSON Errors'] = 'Error No: ' . json_last_error();
            return false;
        }
    }

    /**
     * Checks if the field value meets the encoding rule
     *
     * @param  string $key
     * @param  string $value
     * @param  array  $validateData
     * @return boolean
     */
    private function checkEncoding($key, $value, array $validateData)
    {
        $encodings = explode(',', $validateData[0]);
        if (mb_detect_encoding($value, $encodings)) {
            return true;
        } else {
            $this->errors[$key]['checkEncoding'] = $this->validatorErrors['checkEncoding'];
            return false;
        }
    }

    /**
     * Checks if the field value meets the regular expression rule
     *
     * @param  string $key
     * @param  string $value
     * @param  array  $validateData
     * @return mixed
     */
    private function regEx($key, $value, array $validateData)
    {
        $validated = preg_match($validateData[0], $value);
        if ($validated) {
            return true;
        } else {
            $this->errors[$key]['Regular Expression'] = $this->validatorErrors['regEx'];
            return false;
        }
    }

    /**
     * Gets the errors. If not success!
     *
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }

}
