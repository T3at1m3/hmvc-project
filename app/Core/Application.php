<?php

namespace App\Core;

use App\Core\Router;
use App\Core\Session;

/**
 * The application initialiser class
 *
 * @package Titan\App\Core
 * @author Al.Takev <al.takev@gmail.com>
 * @version 0.0.1
 */
class Application
{
    /**
     * The method that initialises the whole application. The logic in it is
     * reinforced by the try catch block, that catches all exceptions generated
     * by the child classes
     */
    public function run()
    {
        try {
            $this->loadConfigFile();
            $url = trim($_SERVER['REQUEST_URI'], '/');
            Session::start();
            $router = new Router($url);
        } catch (\Exception $e) {
            // You can style this message with HTML;
            echo '<pre style="white-space: pre-wrap;white-space: -moz-pre-wrap; white-space: -pre-wrap; white-space: -o-pre-wrap; word-wrap: break-word;">' . print_r($e->getMessage(), true) . '</pre>';
            exit();
        }
    }

    /**
     * Loads the main config file present into /app/Core/Config
     * @throws Exception If the file is not found
     */
    private function loadConfigFile()
    {
        $configFile = "../app/Core/Config/config.php";
        if (!file_exists($configFile)) {
            throw new \Exception('Config file not exist: ' . $configFile);
        } else {
            require_once $configFile;
        }
    }
}
