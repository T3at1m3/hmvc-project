<?php

namespace App\Core;
/**
 * The Module class
 *
 * Used as a Service Locator
 */
class Module
{
    private $moduleControllerInstance = array();
    private $moduleModelInstance      = array();

    /**
     * Getter for the Module's controller
     * @param  string $module     The module name
     * @param  string $controller The module controller name
     * @return object
     */
    public function getControllerInstance($module, $controller = null)
    {
        if (!$controller) {
            $controller = $module;
        }

        $className = "App\Modules\\" . $module . "\Controller\\" . $controller . "Controller";
        if (!class_exists($className)) {
            throw new \Exception("No such class exists: {$className}");
        }

        if (!isset($this->moduleControllerInstance[$className])) {
            $this->moduleControllerInstance[$className] = new $className();
        }

        return $this->moduleControllerInstance[$className];
    }

    /**
     * Getter for the Module's model
     * @param  string $module The module name
     * @return object
     */
    public function getModelInstance($module)
    {
        $modelName = "App\Modules\\" . $module . "\Model\\" . $module;

        if (!class_exists($modelName)) {
            throw new \Exception("Model doesn't exist: {$modelName}");
        }

        if (!isset($this->moduleModelInstance[$modelName])) {
            $this->moduleModelInstance[$modelName] = new $modelName();
        }

        return $this->moduleModelInstance[$modelName];

    }
}
