<?php

namespace App\Modules\User\Model;

use App\Core\BaseModel;
use App\Core\Config\ValidatorRules;
use App\Core\Lib\TextValidator as Validator;

/**
 * The User data handler class (User Model)
 */
class User extends BaseModel
{
    private $user = [];

    /**
     * When a user registers, this method validates the form input
     * If the input passes, it hashes the user pass and than sends it into the
     * database to be stored
     * @param array $user The user input data
     */
    public function add($user)
    {
        $rules  = ValidatorRules::getRules('registration');
        $val    = new Validator($user, $rules);
        $result = $val->check();
        if (!$result) {
            return [
                'error' => $val->getErrors(),
            ];
        } else {
            $this->user = $user;

            $this->user['password'] = $this->hashUserPass($this->user['password']);

            unset($this->user['password-confirmation']);

            $now = date('Y-m-d H:i:s', time());
            $sql = "INSERT INTO user
                    (username, email, password, active, role, created_at)
                    VALUES
                    (:username, :email, :password, :active, :role, :created_at)";
            $params = [
                'username'   => $this->user['username'],
                'email'      => $this->user['email'],
                'password'   => $this->user['password'],
                'active'     => 0,
                'role'       => 'editor',
                'created_at' => $now,
            ];

            $this->db->prepare($sql, $params)->execute();

            $userID = $this->db->lastInsertID();
            if ($userID) {
                $status = $this->sendActivationMail($userID);
                if ($status) {
                    return [
                        'success' => 'User Created. Chek your email for confirmation link',
                    ];
                } else {
                    return [
                        'error' => 'There was problem with sending the mail, please contact support',
                    ];
                }

            } else {
                return [
                    'error' => 'Cannot create user. Contact support!',
                ];
            }

        }
    }

    /**
     * Hashes the user pass and prepares it to be inserted in the DB
     * @param  string $password The user pass
     * @return string           The hashed string
     */
    private function hashUserPass($password)
    {
        /** Higher cost, better security but more processing power */
        $cost = 10;

        /** Create random salt */
        $salt = strtr(base64_encode(mcrypt_create_iv(16, MCRYPT_DEV_URANDOM)), '+', '.');

        /**
         * Prefix information about the hash so PHP knows how to verify it
         * later.
         * "$2a$" means that we're using the Blowfish. The following two digits
         * are the cost parameter.
         */
        $salt = sprintf("$2a$%02d$", $cost) . $salt;

        /** @var string Hashed user password */
        $hash = crypt($password, $salt);

        return $hash;
    }

    /**
     * Validate the user login input and than checks if this user exists
     * @param  array $user The user input data
     * @return string|array       Error message | Message and the user data
     *                                  to be saved in the Session
     */
    public function checkUserLogin($user)
    {
        $rules = ValidatorRules::getRules('login');
        $val   = new Validator($user, $rules);

        $result = $val->check();
        if (!$result) {
            return [
                'error' => $val->getErrors(),
            ];
        } else {
            $sql = "SELECT id, password FROM user
            WHERE :user IN (username, email) LIMIT 1
            ";
            $params = [
                'user' => $user['user'],
            ];

            $this->db->prepare($sql, $params)->execute();
            $result = $this->db->fetchRowAssoc();

            if ($result && (hash_equals($result['password'], crypt($user['password'], $result['password'])))) {
                $sql = "SELECT id, username, email, role, active
                FROM user WHERE id = :id LIMIT 1";
                $params = [
                    'id' => $result['id'],
                ];
                $this->db->prepare($sql, $params)->execute();
                $userData = $this->db->fetchRowAssoc();
                return [
                    'user'    => $userData,
                    'success' => 'Logged in!',
                ];
            } else {
                return [
                    'error' => 'No such username/email or password',
                ];
            }
        }
    }

    /**
     * Returns the username data by the given ID
     * @param  string|integer $id the user id
     * @return array      The user data
     */
    public function getUsernameByID($id)
    {
        $sql = "SELECT username FROM user
                WHERE id = :id";

        $params = [
            'id' => $id,
        ];

        $this->db->prepare($sql, $params)->execute();

        return $this->db->fetchRowAssoc();
    }

    /**
     * After inserting the user data, sends an activation email, to the
     * registered user
     * @param  integer $id The user id
     * @return boolean     The status
     */
    private function sendActivationMail($id)
    {
        $sql = "SELECT username, email FROM user
                WHERE id = :id";
        $params = [
            'id' => $id,
        ];
        $this->db->prepare($sql, $params)->execute();

        $user = $this->db->fetchRowAssoc();

        $rand = md5(rand(1, 100) . $id . $user['username']);

        $sql = "UPDATE user
                SET code = :code
                WHERE id = :id";
        $params = [
            'id'   => $id,
            'code' => $rand,
        ];

        $this->db->prepare($sql, $params)->execute();

        if ($this->db->rowCount() == 1) {
            $email = $user['email'];

            $subject = "Email Verification mail";
            $headers = "From: titan@titan-gate-mail.org \r\n";
            $headers .= "Reply-To: support@titan-gate-mail.org \r\n";
            $headers .= "MIME-Version: 1.0\r\n";
            $headers .= "Content-Type: text/html; charset=UTF-8\r\n";

            $message = '<html><body>';
            $message .= '<div style="width:550px; background-color:#CC6600; padding:15px; font-weight:bold;">';
            $message .= 'Email Verification mail';
            $message .= '</div>';
            $message .= '<div style="font-family: Arial;">Confiramtion mail have been sent to your email id<br/>';
            $message .= 'click on the below link in your verification mail id to verify your account ';
            $message .= "<a href='http://localhost/titan/user/confirm/$id/$email/$rand'>click</a>";
            $message .= '</div>';
            $message .= '</body></html>';

            $sent = mail($email, $subject, $message, $headers);
        } else {
            $sent = false;
        }

        return $sent;
    }

    /**
     * Activates the user registration
     * @param  string|integer $id The user ID
     * @param  string $code   The activation code from the mail
     * @return integer       1 - success, 0 - fail
     */
    public function activateRegistration($id, $code) {
        $sql = "UPDATE user
                SET
                    active = 1,
                    code   = ''
                WHERE id = :id AND code = :code";
        $params = [
            'id' => $id,
            'code' => $code
        ];

        $this->db->prepare($sql, $params)->execute();

        return $this->db->rowCount();
    }
}
