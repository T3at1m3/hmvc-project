<?php

namespace App\Modules\User\Controller;

use App\Core\Module;
use App\Core\Session;
use App\Core\View;

/**
 * Handles all user interactions
 */
class UserController extends Module
{
    /**
     * The default method
     */
    public function index()
    {
        $user = Session::get('user');
        if (isset($user) && !empty($user)) {
            $template = 'home';
        } else {
            $template = 'login';
        }

        View::template('User', $template, []);
    }

    /**
     * When a user logs in
     */
    public function login()
    {
        $template = 'login';
        $message  = '';
        if ($_POST) {
            $user    = $this->getModelInstance('User');
            $message = $user->checkUserLogin($_POST);
            if (isset($message['success'])) {
                Session::set('user', $message['user']);
                header('Location: ' . PROJECT . 'user/');
            }
        }
        View::template('User', $template, $message);
    }

    /**
     * When a user tries to register
     */
    public function register()
    {
        $message  = [];
        $template = 'register';

        if ($_POST) {
            $user    = $this->getModelInstance('User');
            $message = $user->add($_POST);

            if (isset($message['success'])) {
                $template = 'login';
            }
        }
        View::template('User', $template, $message);
    }

    /**
     * When a user logs out
     */
    public function logout()
    {
        Session::destroy();
        header("Location: " . PROJECT);
    }

    /**
     * When the admin is viewing users
     * @todo Not in the requirements but if you have time, finish it
     */
    public function browse()
    {
        echo 'BROWSE users!';
    }

    /**
     * Get username by the user ID
     * @param  integer|string $id The user id
     * @return array     The user data
     */
    public function getUsernameByID($id)
    {
        $userModel = $this->getModelInstance('User');

        return $userModel->getUsernameByID($id);
    }

    /**
     * When user registers in the application, an email with a link, is sent
     * to his registration email. Upon clicking it redirects here, and the
     * user is activating his account
     *
     * @param  array $data  The user data (id, email, code)
     * @return View
     */
    public function confirm($data)
    {
        $userModel = $this->getModelInstance('User');

        $status = $userModel->activateRegistration($data[0], $data[2]);
        if($status == 1) {
            $message = [ 'success' => 'Successful activation. You can now login with your credentials.' ];
        } else {
            $message = [ 'error' => 'There was a problem with activating your account. Please contact support!'];
        }
        return View::template('User', 'activate', $message);
    }
}
