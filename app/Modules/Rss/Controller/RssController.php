<?php

namespace App\Modules\Rss\Controller;

use App\Core\Module;
use App\Core\View;

/**
 * The Rss Controller
 */
class RssController extends Module
{
    public function index()
    {
        echo $this->buildRSSChannel();
    }

    /**
     * Build the RSS data
     * @return string The Rss string
     */
    public function buildRSSChannel()
    {
        $rssModel = $this->getModelInstance('Rss');
        $articleController = $this->getControllerInstance('Article');

        $articleData = $articleController->getLatestArticles(10);
        $i = 0;
        $prepare = [];
        foreach ($articleData as $article) {
            $prepare['article-' . $i] = $article;
            $i++;
        }

        $rssData = $rssModel->getRSSData($prepare);

        return $rssData;
    }
}