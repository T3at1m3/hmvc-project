<?php

namespace App\Modules\Rss\Model;

use App\Core\BaseModel;
use App\Core\Lib\Array2XML as XML;

class Rss extends BaseModel
{
    /**
     * Parses the php array and converts it to RSS
     * @param  array $data The php data array
     * @return string      THe parsed string
     */
    public function getRSSData($data)
    {
        $xml = XML::createXML('article', $data);
        return $xml->saveXML();
    }
}