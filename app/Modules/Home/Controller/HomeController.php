<?php

namespace App\Modules\Home\Controller;

use App\Core\Module;
use App\Core\View;

/**
 * The home controller (The Index controller)
 */
class HomeController extends Module
{
    /**
     * Handles the "/" root
     *
     * If the settings are not ok, or the articles cannot be displayed,
     * throw an Exception to notify the user
     */
    public function index()
    {
        $article = $this->getControllerInstance('Article');
        try {
            $latestNews = $article->getLatestArticles(10);
        } catch (\Exception $e) {
            echo "You have't set up the config file";
            echo '<br>';
            echo "Please check: app\Core\Config\config.php";
            die();
        }

        View::template('Home', 'index', ['articles' => $latestNews]);
    }
}