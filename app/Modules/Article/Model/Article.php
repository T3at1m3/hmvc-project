<?php

namespace App\Modules\Article\Model;

use App\Core\BaseModel;
use App\Core\Config\ValidatorRules;
use App\Core\Lib\TextValidator as Validator;

/**
 * The Article model
 */

class Article extends BaseModel
{
    private $article = [];

    /**
     * Handles the validation and saves the data in the db
     * @param array $article The article array
     * @return string
     */
    public function add($article)
    {
        $rules  = ValidatorRules::getRules('article');
        $val    = new Validator($article, $rules);
        $result = $val->check();
        if (!$result) {
            return [
                'error' => $val->getErrors(),
            ];
        } else {
            $this->article = $article;

            $now = date('Y-m-d H:i:s', time());

            $sql = "INSERT INTO article
                    (title, text, user_id, image, active, created_at)
                    VALUES
                    (:title, :text, :user_id, :image, :active, :created_at)";
            $params = [
                'title'      => $this->article['title'],
                'text'       => $this->article['article_body'],
                'user_id'    => $this->article['user_id'],
                'image'      => $this->article['article_image'],
                'active'     => 1,
                'created_at' => $now,
            ];
            $this->db->prepare($sql, $params)->execute();

            return [
                'success' => 'Article created',
            ];
        }
    }

    /**
     * Edit an article
     * @param  array $article The article array
     * @return string         The message
     */
    public function edit($article)
    {
        $rules  = ValidatorRules::getRules('article');
        $val    = new Validator($article, $rules);
        $result = $val->check();
        if (!$result) {
            return [
                'error' => $val->getErrors(),
            ];
        } else {
            $this->article = $article;
            $now           = date('Y-m-d H:i:s', time());

            $sql = "UPDATE article
                    SET
                        title = :title,
                        text = :article_body,
                        user_id = :user_id,
                        image = :image
                    WHERE
                        id = :id";
            $params = [
                'title'        => $this->article['title'],
                'article_body' => $this->article['article_body'],
                'user_id'      => $this->article['user_id'],
                'image'        => $this->article['article_image'],
                'id'           => $this->article['id'],
            ];
            $this->db->prepare($sql, $params)->execute();

            return [
                'success' => 'Article updated',
            ];
        }
    }

    /**
     * Delete and article
     * @param  integer $id The article id
     * @return integer     The operation status 1 - success, 0 - false
     */
    public function delete($id)
    {
        $sql = "DELETE FROM article
                WHERE id = :id";
        $params = [
            'id' => $id,
        ];

        $this->db->prepare($sql, $params)->execute();

        return $this->db->rowCount();

    }

    /**
     * Getter for the article
     * @param  integer $id The article id
     * @return array       The article data
     */
    public function getArticle($id)
    {
        $sql = "SELECT * FROM article
                WHERE id = :id";
        $params = ['id' => $id];
        $this->db->prepare($sql, $params)->execute();

        return $this->db->fetchRowAssoc();
    }

    /**
     * Getter for the article by user ID
     * @param  string|integer $userID The user id
     * @return array                  The article data
     */
    public function getArticlesByUserID($userID)
    {
        $sql = "SELECT * FROM article
                WHERE user_id = :user_id
                ORDER BY created_at DESC";
        $params = ['user_id' => $userID];

        $this->db->prepare($sql, $params)->execute();

        return $this->db->fetchAllAssoc();
    }

    /**
     * Getter for all articles
     * @return array  Articles data
     */
    public function getAllArticles()
    {
        $sql = "SELECT * FROM article
                ORDER BY created_at DESC";
        $this->db->prepare($sql)->execute();

        return $this->db->fetchAllAssoc();
    }

    /**
     * Get the latest articles based on the limit param
     * @param  integer $limit The limit
     * @return array          Articles data
     */
    public function getLatestArticles($limit)
    {
        $sql = "SELECT * FROM article
                ORDER BY created_at DESC
                LIMIT :by_limit";
        $params = [
            'by_limit' => $limit,
        ];

        $this->db->prepare($sql, $params)->execute();

        return $this->db->fetchAllAssoc();
    }

    /**
     * Create the article slug
     * @param  integer $id  article id
     * @param  string $text the text to be displayed in the URL
     * @return string       the slug
     */
    public function createArticleURL($id, $text)
    {
        return $id . '-' . $this->slug($this->translateCyrToEng($text));
    }

    /**
     * The URL sanitizer
     * @param  string $url the non-sanitized URL
     * @return string      the sanitized URL
     */
    private function slug($url)
    {
        // Prep string with some basic normalization
        $url = strtolower($url);
        $url = strip_tags($url);
        $url = stripslashes($url);
        $url = html_entity_decode($url);

        // Remove quotes (can't, etc.)
        $url = str_replace('\'', '', $url);

        // Replace non-alpha numeric with hyphens
        $match   = '/[^a-z0-9]+/';
        $replace = '-';
        $url     = preg_replace($match, $replace, $url);
        // Trim the URL
        $url = trim($url, '-');

        return $url;
    }

    /**
     * Transliterator needed for creating URL from Cyrillic to English
     * @param  string $cyrillic The Cyrillic string
     * @return string           The transliterated string
     */
    private function translateCyrToEng($cyrillic)
    {
        $cyr = array('а', 'б', 'в', 'г', 'д', 'е', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у',
            'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ь', 'ю', 'я', 'А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У',
            'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ъ', 'Ь', 'Ю', 'Я');
        $lat = array('a', 'b', 'v', 'g', 'd', 'e', 'j', 'z', 'i', 'yi', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u',
            'f', 'h', 'c', 'ch', 'sh', 'sht', 'a', 'y', 'iu', 'ya', 'A', 'B', 'V', 'G', 'D', 'E', 'J',
            'Z', 'I', 'YI', 'K', 'L', 'M', 'N', 'O', 'P', 'R', 'S', 'T', 'U',
            'F', 'H', 'C', 'Ch', 'Sh', 'Sht', 'A', 'Y', 'Iu', 'Ya');
        $textCyr = str_replace($cyr, $lat, $cyrillic);
        return $textCyr;
    }
}
