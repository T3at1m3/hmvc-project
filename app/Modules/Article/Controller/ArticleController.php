<?php

namespace App\Modules\Article\Controller;

use App\Core\Lib\Upload;
use App\Core\Module;
use App\Core\Session;
use App\Core\View;

use Dompdf\Dompdf;
use Dompdf\Options;

/**
 * All article interactions are handled with the controller
 *
 * The form of the dependency injection here is done via the constructor.
 * Every time the class is instantiated the article model is created too
 */

class ArticleController extends Module
{
    private $articleModel;

    public function __construct()
    {
        $this->articleModel = $this->getModelInstance('Article');
    }

    /**
     * Add an article
     */
    public function add()
    {
        $user = Session::get('user');
        if (!isset($user) && empty($user)) {
            return View::template('User', 'login', []);
        }

        if ($user['active'] != '1') {
            return View::template('User', 'home', ['error' => 'You must active your account first']);
        }

        $data     = [];
        $template = 'add';
        if ($_POST) {
            if ($_FILES['file_upload']) {
                $uploader = Upload::factory(PROJECT_DIR . 'public/uploads');
                $uploader->file($_FILES['file_upload']);
                /** In MB */
                $uploader->set_max_file_size(1);

                $uploader->set_allowed_mime_types([
                    'image/gif',
                    'image/jpeg',
                    'image/png',
                ]);
                $image = $uploader->upload();
            }

            if (!empty($image['errors'])) {
                $html = 'Cannot upload: <br>';
                foreach ($image['errors'] as $imageError) {
                    $html .= '<p>' . $imageError . '</p>';
                }
                $data = ['error' => $html];
            } else {
                $articleData                  = $_POST;
                $articleData['article_image'] = $image['filename'];
                $articleData['user_id']       = $_SESSION['user']['id'];

                $status = $this->articleModel->add($articleData);
                $data   = (isset($status['error'])) ? ['input' => $status['error']] : $status;
            }
        }

        View::template('Article', $template, $data);
    }

    /**
     * Edit an article
     *
     * Only an administrator can edit articles
     * @param string\integer $id The article ID
     */
    public function edit($id)
    {
        $user = Session::get('user');

        if (!isset($user['id']) && empty($user['id'])) {
            header("Location: " . PROJECT . 'user/login/');
        }

        if (isset($user) && ($user['role'] !== 'admin')) {
            return View::template('Home', 'error', ['error' => 'You have no permissions to process this action']);
        }
        $data = $this->articleModel->getArticle($id);

        $template = 'add';

        if ($_POST) {

            if ($_FILES['file_upload']['error'] === 0) {
                $uploader = Upload::factory('titan/public/uploads');
                $uploader->file($_FILES['file_upload']);
                /** In MB */
                $uploader->set_max_file_size(1);

                $uploader->set_allowed_mime_types([
                    'image/gif',
                    'image/jpeg',
                    'image/png',
                ]);
                $image = $uploader->upload();
            } else {
                $image['filename'] = $data['image'];
            }
            $articleData                  = $_POST;
            $articleData['id']            = $data['id'];
            $articleData['article_image'] = $image['filename'];
            $articleData['user_id']       = $_SESSION['user']['id'];

            $status   = $this->articleModel->edit($articleData);
            $data     = (isset($status['error'])) ? ['input' => $status['error']] : $status;
            $template = (isset($status['error'])) ? 'add' : header("Location: " . PROJECT . 'article/view/' . $id);
        } else {
            $data = ['edit' => $data];
        }
        View::template('Article', $template, $data);
    }

    /**
     * The delete action
     * @param  string\integer $id The article id
     */
    public function delete($id)
    {
        $id     = (int) $id;
        $status = $this->articleModel->delete($id);
        $status = ($status === 1) ? ['success' => 'Article deleted!'] : ['error' => 'Cannot delete this article!'];

        $this->browse($status);
    }

    /**
     * View an article
     * @param  string\integer $id The article id
     */
    public function view($id)
    {
        $passedID       = explode('-', $id, 2);
        $articleID      = $passedID[0];
        $userController = $this->getControllerInstance('User');

        $article = $this->articleModel->getArticle($articleID);

        $article['user_data'] = $userController->getUsernameByID($article['user_id']);
        View::template('Article', 'single', ['article' => $article]);
    }

    /**
     * The article listing
     * @param  boolean $status The status of the current message
     */
    public function browse($status = false)
    {
        $user = Session::get('user');

        if ($user['role'] === 'admin') {
            $data     = $this->articleModel->getAllArticles();
            $template = 'browse_admin';
        } else {
            $userID   = $user['id'];
            $data     = $this->articleModel->getArticlesByUserID($userID);
            $template = 'browse';
        }

        $articles = [];
        $i        = 0;
        foreach ($data as $article) {
            $articles[$i]        = $article;
            $articles[$i]['url'] = $this->articleModel->createArticleURL($article['id'], $article['title']);
            $i++;
        }
        if (empty($articles)) {
            $status = ['info' => "You haven't published anything yet"];
        }
        View::template('Article', $template, [
            'articles' => $articles,
            'user'     => $user,
            'message'  => $status,
        ]);
    }

    /**
     * Converts an article into a PDF
     * @param  string|integer $id The article id
     */
    public function convert($id)
    {
        $id = (int) $id;
        $url = PROJECT . 'article/view/' . $id;

        $options = new Options();
        $options->set('isRemoteEnabled', true);
        $dompdf = new Dompdf($options);
        $dompdf->loadHtmlFile($url);

        $dompdf->render();
        $dompdf->stream();
    }

    /**
     * Listing for the latest published articles
     * @param  integer $limit The limit of the items
     * @return array          The articles array
     */
    public function getLatestArticles($limit = 10)
    {
        $data     = $this->articleModel->getLatestArticles($limit);
        $articles = [];
        $i        = 0;

        $userController = $this->getControllerInstance('User');

        foreach ($data as $article) {
            $articles[$i]        = $article;
            $articles[$i]['url'] = $this->articleModel->createArticleURL($article['id'], $article['title']);

            $userData = $userController->getUsernameByID($data[$i]['user_id']);

            $articles[$i]['user_data'] = ($userData) ? $userData : ['username' => 'Unknown'];
            $i++;
        }

        return $articles;
    }
}
