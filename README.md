# README #

## Basic News Publishing Portal ##

* A public news publishing system where people can report or read news
* Every user can view articles, only registered can publish
* Has role based access

### How do I get set up? ###

** These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. **

* Setup a localhost
* Create a database with name of your choosing and charset UTF-8


```
#!cmd

cd <project-name>
composer install

```

Open <project-name>\app\Core\Config\config.php

If you wish to change the project folder, feel free to do so, but don't forget to change it on line 10 of the file


```
#!php

$project = 'titan';

```


The only database driver this project supports is PDOMySQL, so leave it for now.

On line 26 you must set your database connection.

```
#!php
$connectionOptions = [
    "host"     => 'localhost',
    "user"     => 'db-user',
    "pass"     => 'db-pass',
    "database" => 'db-name',
    "charset"  => "utf8",
];

```
There are 3 methods

```
#!php
$settings->setDBTabels(false);

$settings->setDefaultUser($userData, false);

$settings->populatearticlesTable(false);

```
On the very first run of the application in order to migrate the tables on the setDBTables use the true parameter

After the first run, change it again to false

The other two methods are optional.
The first sets default users, and the second populates dummy article data into the databse

* Tests

Unit testing is not supported

### Technologies ###

The project uses php extentions and vendor libraries:

1. PDO
2. dompdf
3. fzaninotto/Faker
4. psr-4
5. Simple PHP Upload
6. Array2XML

### Design Patters ###

1. Hierarchical MVC
2. Service Locator
3. Singleton

### Who do I talk to? ###

* al.takev@gmail.com